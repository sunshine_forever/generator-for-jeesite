package com.featherlike.feather.generator.entity;

public class Field {

	private String name;
	private String type;
	private String comment;
	private int length;
	private String listPage;
	private String queryPage;
	private String modifyPage;
	private String asLink;
	private String dictType;

	public Field(String name, String type, String comment, int length,
			String listPage, String queryPage, String modifyPage,
			String asLink, String dictType) {
		this.name = name;
		this.type = type;
		this.comment = comment;
		this.length = length;
		this.listPage = listPage;
		this.queryPage = queryPage;
		this.modifyPage = modifyPage;
		this.asLink = asLink;
		this.dictType = dictType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getListPage() {
		return listPage;
	}

	public void setListPage(String listPage) {
		this.listPage = listPage;
	}

	public String getQueryPage() {
		return queryPage;
	}

	public void setQueryPage(String queryPage) {
		this.queryPage = queryPage;
	}

	public String getModifyPage() {
		return modifyPage;
	}

	public void setModifyPage(String modifyPage) {
		this.modifyPage = modifyPage;
	}

	public String getAsLink() {
		return asLink;
	}

	public void setAsLink(String asLink) {
		this.asLink = asLink;
	}

	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}
}
